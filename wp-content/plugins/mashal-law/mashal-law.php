<?php
/*
Plugin Name: Mashal Law Options
Plugin URI: http://www.example.com
Description: Mashal Law Theme Setup Options
Version: 1.0
Author: MCR Warrington
*/

/*
* Define the array where everything is saved in the database
*/
define('MASHAL_OPTIONS_DATA', 'mashal_options');

/*
* Actions & Filters
*/
register_activation_hook(__FILE__, 'mashal_law_setup');
add_action('admin_menu', 'mashal_law_options');

//function mashal_law_setup() {
//    return true;
//}

function mashal_default_capability($capability) {
    return 'manage_options';
}
add_filter('mashal_options_capability', 'mashal_default_capability');

function mashal_law_options() {
    $capability = apply_filters('mashal_options_capability', 'manage_options');
    add_options_page(__('ניהול תבנית'), __('ניהול תבנית'), $capability, 'mashal_law', 'mashal_options_form');
}

function mashal_options_form() {

    $mashal_law_data = get_option(MASHAL_OPTIONS_DATA);
    if (!empty($mashal_law_data))
        $mashal_law_data = json_decode($mashal_law_data);

    echo '
		<div class="wrap"><div id="icon-options-general" class="icon32"><br /></div>
		<h2>'.__('Mashal Law Options').'</h2>';

    if (!empty($_POST) &&
        check_admin_referer('mashal_law_action', 'mashal_law_nonce_field') &&
        wp_verify_nonce($_POST['mashal_law_nonce_field'], 'mashal_law_action')) {

        $data = $_POST['data'];

        // Features
        $data = json_encode($data);

        update_option(MASHAL_OPTIONS_DATA, $data);
        $mashal_law_data = json_decode($data);
    }

    echo '<form method="post" action="">';
    echo '<br />';

    $args = array(
        'type'                     => 'post',
        'child_of'                 => 0,
        'parent'                   => '',
        'orderby'                  => 'name',
        'order'                    => 'ASC',
        'hide_empty'               => 0,
        'hierarchical'             => 1,
        'exclude'                  => get_cat_ID('testimonial').','.get_cat_ID('system'),
        'include'                  => '',
        'number'                   => '',
        'taxonomy'                 => 'category',
        'pad_counts'               => false 
    ); 

    $cats = get_categories( $args );

    // get posts for each category (buy,sell and rent)
    if(!empty($mashal_law_data->buy_category))
        $buy = $mashal_law_data->buy_category;
    else
        $buy = $cats[0]->cat_name;

    if(!empty($mashal_law_data->sell_category))
        $sell = $mashal_law_data->sell_category;
    else
        $sell = $cats[0]->cat_name;

    if(!empty($mashal_law_data->rent_category))
        $rent = $mashal_law_data->rent_category;
    else
        $rent = $cats[0]->cat_name;

    echo '
<table class="form-table">
    <tbody>
        <tr>
            <th scope="row">Form Shortcodes</th>
            <td>
                <fieldset>
                    <legend class="screen-reader-text">
                        <span>Form Shortcodes</span>
                    </legend>
                    <label>
                        <input type="text" name="data[signup_shortcode]" value="'.(!empty($mashal_law_data->signup_shortcode) ? htmlentities(stripslashes($mashal_law_data->signup_shortcode)) : '').'" class="regular-text" /> Signup Strip Form Shortcode
                    </label>
                    <br />
                    <label>
                        <input type="text" name="data[sidebar_shortcode]" value="'.(!empty($mashal_law_data->sidebar_shortcode) ? htmlentities(stripslashes($mashal_law_data->sidebar_shortcode)) : '').'" class="regular-text" /> Sidebar Form Shortcode
                    </label>
                    <br />
                    <label>
                        <input type="text" name="data[newsstrip_shortcode]" value="'.(!empty($mashal_law_data->newsstrip_shortcode) ? htmlentities(stripslashes($mashal_law_data->newsstrip_shortcode)) : '').'" class="regular-text" /> News Strip Form Shortcode
                    </label>
                </fieldset>
            </td>
        </tr>
        <tr>
            <th scope="row">Buy Page Options</th>
            <td>
                <fieldset>
                    <legend class="screen-reader-text">
                        <span>Buy Page Options</span>
                    </legend>';
                    if($cats) {
                        echo '<label>';
                        echo '<select name="data[buy_category]" id="buy_category">';                    
                        foreach($cats as $cat) {
                            echo '<option value="'.$cat->cat_name.'"';
                            if(!empty($mashal_law_data->buy_category) && $mashal_law_data->buy_category == $cat->cat_name)
                                echo ' selected="selected"';
                            echo '>'.$cat->cat_name.'</option>';
                        }
                        echo '</select> Buy Category</label>';
                    }                  
echo '<br />
                    <label>';
                    query_posts( 'cat='.get_cat_ID($buy) );
                    if(have_posts()) {
                        echo '<select name="data[buy_page_related_article_1]" id="buy_page_related_article_1">';
                        while (have_posts()) : the_post();
                            echo '<option value="'.get_the_ID().'"';
                            if(!empty($mashal_law_data->buy_page_related_article_1) && $mashal_law_data->buy_page_related_article_1 == get_the_ID())
                                echo ' selected="selected"';
                            echo '>'.the_title('', '', false).'</option>';
                        endwhile;
                         echo '</select> Buy page article 1</label>';
                    }
                        
                    echo '<br />
                    <label>';
                    query_posts( 'cat='.get_cat_ID($buy) );
                    if(have_posts()) {
                        echo '<select name="data[buy_page_related_article_2]" id="buy_page_related_article_2">';
                        while (have_posts()) : the_post();
                            echo '<option value="'.get_the_ID().'"';
                            if(!empty($mashal_law_data->buy_page_related_article_2) && $mashal_law_data->buy_page_related_article_2 == get_the_ID())
                                echo ' selected="selected"';
                            echo '>'.the_title('', '', false).'</option>';
                        endwhile;
                         echo '</select> Buy page article 2</label>';
                    }
                        
                    echo '<br />
                    <label>';
                    query_posts( 'cat='.get_cat_ID($buy) );
                    if(have_posts()) {
                        echo '<select name="data[buy_page_related_article_3]" id="buy_page_related_article_3">';
                        while (have_posts()) : the_post();
                            echo '<option value="'.get_the_ID().'"';
                            if(!empty($mashal_law_data->buy_page_related_article_3) && $mashal_law_data->buy_page_related_article_3 == get_the_ID())
                                echo ' selected="selected"';
                            echo '>'.the_title('', '', false).'</option>';
                        endwhile;
                         echo '</select> Buy page article 3</label>';
                    }
                        
                    echo '</fieldset>
            </td>
        </tr>
        <tr>
            <th scope="row">Sell Page Options</th>
            <td>
                <fieldset>
                    <legend class="screen-reader-text">
                        <span>Sell Page Options</span>
                    </legend>';
                    if($cats) {
                        echo '<label>';
                        echo '<select name="data[sell_category]" id="sell_category">';                    
                        foreach($cats as $cat) {
                            echo '<option value="'.$cat->cat_name.'"';
                            if(!empty($mashal_law_data->sell_category) && $mashal_law_data->sell_category == $cat->cat_name)
                                echo ' selected="selected"';
                            echo '>'.$cat->cat_name.'</option>';
                        }
                        echo '</select> Sell Category</label>';
                    }                  
echo '<br />
                    <label>';
                    query_posts( 'cat='.get_cat_ID($sell) );
                    if(have_posts()) {
                        echo '<select name="data[sell_page_related_article_1]" id="sell_page_related_article_1">';
                        while (have_posts()) : the_post();
                            echo '<option value="'.get_the_ID().'"';
                            if(!empty($mashal_law_data->sell_page_related_article_1) && $mashal_law_data->sell_page_related_article_1 == get_the_ID())
                                echo ' selected="selected"';
                            echo '>'.the_title('', '', false).'</option>';
                        endwhile;
                         echo '</select> Sell page article 1</label>';
                    }
                        
                    echo '<br />
                    <label>';
                    query_posts( 'cat='.get_cat_ID($sell) );
                    if(have_posts()) {
                        echo '<select name="data[sell_page_related_article_2]" id="sell_page_related_article_2">';
                        while (have_posts()) : the_post();
                            echo '<option value="'.get_the_ID().'"';
                            if(!empty($mashal_law_data->sell_page_related_article_2) && $mashal_law_data->sell_page_related_article_2 == get_the_ID())
                                echo ' selected="selected"';
                            echo '>'.the_title('', '', false).'</option>';
                        endwhile;
                         echo '</select> Sell page article 2</label>';
                    }
                        
                    echo '<br />
                    <label>';
                    query_posts( 'cat='.get_cat_ID($sell) );
                    if(have_posts()) {
                        echo '<select name="data[sell_page_related_article_3]" id="sell_page_related_article_3">';
                        while (have_posts()) : the_post();
                            echo '<option value="'.get_the_ID().'"';
                            if(!empty($mashal_law_data->sell_page_related_article_3) && $mashal_law_data->sell_page_related_article_3 == get_the_ID())
                                echo ' selected="selected"';
                            echo '>'.the_title('', '', false).'</option>';
                        endwhile;
                         echo '</select> Sell page article 3</label>';
                    }
                        
                    echo '</fieldset>
            </td>
        </tr>
        <tr>
            <th scope="row">Rent Page Options</th>
            <td>
                <fieldset>
                    <legend class="screen-reader-text">
                        <span>Rent Page Options</span>
                    </legend>';
                    if($cats) {
                        echo '<label>';
                        echo '<select name="data[rent_category]" id="rent_category">';                    
                        foreach($cats as $cat) {
                            echo '<option value="'.$cat->cat_name.'"';
                            if(!empty($mashal_law_data->rent_category) && $mashal_law_data->rent_category == $cat->cat_name)
                                echo ' selected="selected"';
                            echo '>'.$cat->cat_name.'</option>';
                        }
                        echo '</select> Rent Category</label>';
                    }                
echo '<br />
                    <label>';
                    query_posts( 'cat='.get_cat_ID($rent) );
                    if(have_posts()) {
                        echo '<select name="data[rent_page_related_article_1]" id="rent_page_related_article_1">';
                        while (have_posts()) : the_post();
                            echo '<option value="'.get_the_ID().'"';
                            if(!empty($mashal_law_data->rent_page_related_article_1) && $mashal_law_data->rent_page_related_article_1 == get_the_ID())
                                echo ' selected="selected"';
                            echo '>'.the_title('', '', false).'</option>';
                        endwhile;
                         echo '</select> Rent page article 1</label>';
                    }
                        
                    echo '<br />
                    <label>';
                    query_posts( 'cat='.get_cat_ID($rent) );
                    if(have_posts()) {
                        echo '<select name="data[rent_page_related_article_2]" id="rent_page_related_article_2">';
                        while (have_posts()) : the_post();
                            echo '<option value="'.get_the_ID().'"';
                            if(!empty($mashal_law_data->rent_page_related_article_2) && $mashal_law_data->rent_page_related_article_2 == get_the_ID())
                                echo ' selected="selected"';
                            echo '>'.the_title('', '', false).'</option>';
                        endwhile;
                         echo '</select> Rent page article 2</label>';
                    }
                        
                    echo '<br />
                    <label>';
                    query_posts( 'cat='.get_cat_ID($rent) );
                    if(have_posts()) {
                        echo '<select name="data[rent_page_related_article_3]" id="rent_page_related_article_3">';
                        while (have_posts()) : the_post();
                            echo '<option value="'.get_the_ID().'"';
                            if(!empty($mashal_law_data->rent_page_related_article_3) && $mashal_law_data->rent_page_related_article_3 == get_the_ID())
                                echo ' selected="selected"';
                            echo '>'.the_title('', '', false).'</option>';
                        endwhile;
                         echo '</select> Rent page article 3</label>';
                    }
                        
                    echo '</fieldset>
            </td>
        </tr>
        <tr>
            <th scope="row">General Page Options</th>
            <td>
                <fieldset>
                    <legend class="screen-reader-text">
                        <span>General Page Options</span>
                    </legend>';
                    if($cats) {
                        echo '<label>';
                        echo '<select name="data[general_category]" id="general_category">';                    
                        foreach($cats as $cat) {
                            echo '<option value="'.$cat->cat_name.'"';
                            if(!empty($mashal_law_data->general_category) && $mashal_law_data->general_category == $cat->cat_name)
                                echo ' selected="selected"';
                            echo '>'.$cat->cat_name.'</option>';
                        }
                        echo '</select> General Category</label>';
                    }                
echo '</fieldset>
            </td>
        </tr>
        <tr>
            <th scope="row">Social URLs</th>
            <td>
                <fieldset>
                    <legend class="screen-reader-text">
                        <span>Social URLs</span>
                    </legend>
                    <label>
                        <input type="text" name="data[facebook_url]" value="'.(!empty($mashal_law_data->facebook_url) ? htmlentities(stripslashes($mashal_law_data->facebook_url)) : '').'" class="regular-text" /> Facebook URL
                    </label>
                    <br />
                    <label>
                        <input type="text" name="data[linkdin_url]" value="'.(!empty($mashal_law_data->linkdin_url) ? htmlentities(stripslashes($mashal_law_data->linkdin_url)) : '').'" class="regular-text" /> LinkedIn URL
                    </label>
                </fieldset>
            </td>
        </tr>
    </tbody>
</table>
';
    wp_nonce_field('mashal_law_action','mashal_law_nonce_field');

    echo '
		<p class="submit">
			<input type="submit" name="submit" id="submit" class="button-primary" value="'.__("Save Changes").'" />
		</p>
	</form>
</div>';


}

function mashal_law_page_view($attrs){
    return ''; // All visuals are in page-pricing.php template, due some problems with Header/Footer
}

add_shortcode('mashal_law_page_view', 'mashal_law_page_view');
