<?php
/**
 * Mashal_law functions and definitions
 *
 * @package Mashal_law
 */
global $closeRowDiv, $hottag, $testimonialCat, $buySlug, $sellSlug, $rentSlug;

$hottag = "חדש";
$testimonialCat = "לקוחות ממליצים";
$buySlug = "השכרת-דירה";
$sellSlug = "מכירת-דירה";
$rentSlug = "השכרת-דירה";
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'mashal_law_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function mashal_law_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Mashal_law, use a find and replace
	 * to change 'mashal-law' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'mashal-law', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'mashal-law' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'mashal_law_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	add_theme_support( 'post-thumbnails' );
	// set thumbnail size
	set_post_thumbnail_size( 200, 120, true ); 
	//add_image_size( 'custom-size', 220, 122 ); // 220 pixels wide by 180 pixels tall, soft proportional crop mode
}
endif; // mashal_law_setup
add_action( 'after_setup_theme', 'mashal_law_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function mashal_law_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'mashal-law' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'mashal_law_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mashal_law_scripts() {
	wp_enqueue_style( 'mashal-law-style', get_stylesheet_uri() );

	wp_enqueue_script( 'mashal-law-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'mashal-law-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mashal_law_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



// Mashal Law Custom Functions

// Abandoned - removing these removes the <p> wrapping of content, which can be annoying at times
//remove_filter( 'the_content', 'wpautop' );
//remove_filter( 'the_excerpt', 'wpautop' );
add_theme_support( 'post-thumbnails' );


function ml_get_text( $text ) {
	if(substr($text,0,9) == "Private: ")
		return substr($text,9);
	else
		return $text;
}

//* shortcodes *//

//[pageheading]
function pageheading_func( $atts, $content = null ){
	
	return '<div class="article-title"><h1>'.$content.'</h1></div><div class="article-text">';
}
add_shortcode( 'pageheading', 'pageheading_func' );

//[heading]
function heading_func( $atts, $content = null ){
	
	return '<h3>'.$content.'</h3>';
}
add_shortcode( 'heading', 'heading_func' );

//[maincontent]
function maincontent_func( $atts, $content = null ){
	global $closeRowDiv;

	$content = wpautop($content, false);

	$a = shortcode_atts( array(
        'heading' => '', 
    ), $atts );
    $output = '';

    if($a['heading'] != '') {
    	$output = '<h3>'.$a['heading'].'</h3>';

    }

    if($closeRowDiv == true) {
    	$closeRowDiv = false;
    	$output .= '</div>';
    }
    $output .= '<div class="row"><div class="short-main-content-text col-lg-9 col-md-9">'.$content.'</div>';
    $closeRowDiv = true;
	return $output;
}
add_shortcode( 'maincontent', 'maincontent_func' );

//[sidecontent]
function sidecontent_func( $atts, $content = null ){
	$content = wpautop($content, false);
	return '<div class="short-side-content-text col-lg-3 col-md-3">'.$content.'</div>';
}
add_shortcode( 'sidecontent', 'sidecontent_func' );

//[credit]
function credit_func( $atts, $content = null ){
	global $pageType;

	$content = wpautop($content, false);

	if($pageType != "testimonials-page")
		$doLink = true;
	else
		$doLink = false;
	
	$ret = '<div class="row">
					<div class="testimonial-credit col-lg-4 col-lg-offset-5 col-md-4 col-md-offset-5 col-sm-4 col-sm-offset-5">'.$content.'</div>';
    if($doLink) {
    	$ret .= '<div class="testimonials-section-permalink col-lg-3 col-md-3 col-sm-3"><a href="'.get_category_link( get_cat_ID( 'לקוחות ממליצים'  )).'">המלצות נוספות</a>'.'</div>';
    }              
        $ret .= '</div>'; 
                
	return $ret;
}
add_shortcode( 'credit', 'credit_func' );

// fix contact-7 insistence on width=40 columns
function fix_columns($content) {
	$content = do_shortcode( $content );	// convent shortcode into page text
	$content = str_replace('<p>','', $content);	// remove <p> tag
	$content = str_replace('</p>','', $content);	// remove </p> tag
	//$content = preg_replace('/<img.*>/i', '', $content);	// remove <image>*contents*</image> - JS is putting it back anyway
    return str_replace('size="40"','',$content);
}

//[title]
function title_func( $atts, $content = null ){
	
	return '<h3>'.$content.'</h3>';
}
add_shortcode( 'title', 'title_func' );

//[image]
function image_func( $atts, $content = null ){
	global $pageType,$closeRowDiv;
	// if home-page then it's a blog post, so wrap in <div row else it's a page in which case div not wanted.
	$output = '';
	if($pageType == 'home-page') {
		if($closeRowDiv == true) {
	    	//$closeRowDiv = false;
	    	$output = '</div>';
	    }
		return $output.'<div class="row"><div class="short-main-content-text col-lg-9 col-md-9 image-container"><img src="'.$content.'"></img></div>';
	}
	return $output.'<img src="'.$content.'"></img>';
}
add_shortcode( 'image', 'image_func' );

function everything_in_tags($string, $tagname)
{
    $pattern = "#[[".$tagname."]*](.*?)[[/".$tagname."]]#i";
    preg_match($pattern, $string, $matches);

    if($matches[0]){
    	$ret = str_replace('['.$tagname.']','', $matches[0]);
    	$ret = str_replace('[/'.$tagname.']','', $ret);
    	return $ret;
    }
}

function testimonial_class_names( $classes ) {
	// add 'class-name' to the $classes array
	// * page page-id-18 page-template page-template-page-templatestestimonials-page-php logged-in admin-bar custom-background customize-support
	// archive category category-testimonial category-10 logged-in admin-bar custom-background customize-support
	// testimonials-content
	$classes[] = 'class-name';
	// return the $classes array
	return $classes;
}

function content_excerpt($excerpt, $charlength = 200, $strip = false) {
	$charlength++;

	if($strip)
		$excerpt = strip_tags_content($excerpt);

	$ret = '';

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$ret .= mb_substr( $subex, 0, $excut );
		} else {
			$ret .= $subex;
		}
		$ret .= '...';
	} else {
		$ret .= $excerpt;
	}

	return $ret;
}


function strip_tags_content($text) {
	$text = str_replace('[maincontent]','',$text);
	$text = str_replace('[/maincontent]','',$text);
	$text = str_replace('[image]','',$text);
	$text = str_replace('[/image]','',$text);
	$text = str_replace('[sidecontent]','',$text);
	$text = str_replace('[/sidecontent]','',$text);

	return $text;
} 