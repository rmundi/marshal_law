<?php
/**
 * Template Name: Mashal Law Blog
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
global $post, $pageClass, $isBuy, $isSell, $isGeneral, $isLet, $hottag, $buySlug, $sellSlug, $rentSlug;

$pageClass = 'blog';

if (is_category( )) {
	$category = get_query_var('cat');
	$yourcat = get_category ($category);

	if( urldecode($yourcat->slug) == 'לקוחות-ממליצים') {
		// Testimonial page, redirect
		get_template_part( 'category-testimonial' );
		die();
	}

	switch(urldecode($yourcat->slug)) {
		/* rent/sell/buy are now actually defunct and determined dynamically in ml-blogform for ticking*/
		case "rent":
		case "השכרת-דירה-מהבלוג-שלנו":
		case $rentSlug:
		case "%d7%94%d7%a9%d7%9b%d7%a8%d7%aa-%d7%93%d7%99%d7%a8%d7%94-%d7%9e%d7%94%d7%91%d7%9c%d7%95%d7%92-%d7%a9%d7%9c%d7%a0%d7%95":
					$isLet = true;
					break;
		case "sell":
		case "מכירת-דירה-מהבלוג-שלנו":
		case $sellSlug:
		case "%d7%9e%d7%9b%d7%99%d7%a8%d7%aa-%d7%93%d7%99%d7%a8%d7%94-%d7%9e%d7%94%d7%91%d7%9c%d7%95%d7%92-%d7%a9%d7%9c%d7%a0%d7%95":
					$isSell = true;
					break;
		case "buy":
		case "קניית-דירה-מהבלוג-שלנו":
		case $buySlug:
		case "%d7%a7%d7%a0%d7%99%d7%99%d7%aa-%d7%93%d7%99%d7%a8%d7%94-%d7%9e%d7%94%d7%91%d7%9c%d7%95%d7%92-%d7%a9%d7%9c%d7%a0%d7%95":
					$isBuy = true;
					break;
		
		case "general":
		case "blog":
		case "מהבלוג-שלנו":
		case "%d7%9e%d7%94%d7%91%d7%9c%d7%95%d7%92-%d7%a9%d7%9c%d7%a0%d7%95":
					$isGeneral = true;
					break;
	}
}

$first = true;

get_header();

get_template_part( 'ml-news-strip' );

//if($yourcat->slug == 'blog')
//	$args = array( 'posts_per_page' => 5, 'order'=> 'ASC', 'orderby' => 'title', 'cat' => '-'.get_cat_ID('testimonial').',-'.get_cat_ID('system')  );
//else
//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }
$args = array( 
		'posts_per_page' => -1, 
		'order'=> 'ASC', 
		'orderby' => 
		'title', 
		'cat' => $yourcat->cat_ID
	);

$count = get_posts( $args );

$numPostsPerPage = 10;

$linkargs = array(
	'base'               => '%_%',
	'format'             => '?page=%#%',
	'total'              => ceil(count($count)/$numPostsPerPage),
	'current'            => 0,
	'show_all'           => False,
	'end_size'           => 3,
	'mid_size'           => 3,
	'prev_next'          => True,
	'prev_text'          => __('« Previous'),
	'next_text'          => __('Next »'),
	'type'               => 'plain',
	'add_args'           => False,
	'add_fragment'       => '',
	'before_page_number' => '',
	'after_page_number'  => ''
); 
$args = array( 
	'posts_per_page' => $numPostsPerPage, 
	'order'=> 'ASC', 
	'orderby' => 
	'title', 
	'cat' => $yourcat->cat_ID,
	'paged' => $paged
);

$postslist = get_posts( $args );

if(!$postslist) { ?>
	<article <?php post_class( array('article-content', 'blogpost-content',) ); ?>>
		<div class="empty-screen">

		</div>
	</article>
<?php } 

foreach ( $postslist as $post ) :
  	setup_postdata( $post );

  	// get image
	$imageData = $post->post_content;
  	$featuredImageUrl = get_the_post_thumbnail($post->ID, '', array( 'class' => "thumb-img" ));
  	if(!$featuredImageUrl) 
		$featuredImageUrl = '<img class="thumb-img" src="'.get_template_directory_uri().'/images/placeholder.png'.'" />';
//	$cats = wp_get_post_categories($post->ID);
	$isHot = has_tag( $hottag );
/*
	foreach($cats as $c) {
		$cat = get_category( $c );
		if($cat->name == "hot") {
			$isHot = true;
		} 
	}
*/
	if($first) {
		$first = false; ?>
		<article <?php post_class( array('article-content', 'blogpost-content',) ); ?>>
			<div class="container">
				<div class="col-lg-3 col-md-3">
					<?php get_template_part( 'ml-blogform' ); ?>
		        <div class="col-lg-9 col-md-9">
		        	<h2>מהבלוג שלנו<?php
		        	if(!$isGeneral) {
		        		echo ', '.str_replace('מהבלוג שלנו', '', str_replace('-', ' ',urldecode($yourcat->slug)));
		        	}
		        	?></h2>					
	<?php } ?> 
					<div class="blog-container">
						<div class="row article-post <?php echo ($isHot ? 'hotbox' : '')?>">
							<?php echo ($isHot ? '<div class="hotindicator">חם בבלוג</div>' : '')?>
						
							<div class="col-lg-3">
								<?php echo $featuredImageUrl; ?>
							</div>
							<div class="col-lg-9">
								<a href="<?php echo get_permalink($post->ID); ?>"><h2>
									<?php the_title(); ?>
								</h2></a>
								<div>
									<?php 
										$data = $post->post_content; 
										echo apply_filters('the_content', content_excerpt($data, 200, true));
									?>
								</div>
								<div class="links-row">
									<a href="<?php echo get_permalink($post->ID); ?>">קרא עוד</a>
								</div>
							</div>
						</div>
					</div>
<?php

endforeach; 
wp_reset_postdata();
echo paginate_links( $linkargs ); ?> 
				</div>
				
			</div>
				
		</article>
<?php
get_template_part( 'ml-form-strip' );
get_footer();