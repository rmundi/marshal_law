<?php
/**
 * Template Name: Mashal Law Home
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
global $pageClass, $testimonialCat;

$pageClass = 'home';

get_header(); 
query_posts('name=home-page-post');
have_posts();
the_post();
$featuredImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
$title = ml_get_text(get_the_title());
?>

<?php get_template_part( 'ml-news-strip' ); ?>

<div class="top-hero home">
	<div class="container">
		<div class="element-content row">
			<div class="hero-img col-lg-4 col-md-4 ">
                <img src="<? echo $featuredImageUrl ?>" alt="" />
            </div>
            <div class="hero-text col-lg-6 col-lg-offset-2 col-md-6 col-md-offset-2">
                <h2>
                    <?php echo $title; ?>
                </h2>
                <p>
                    <?php
                    echo wpautop(do_shortcode( $post->post_content ), false); 
                    //the_content(); 
                    ?>
                </p>
                <a class="readmore-link" href="about/">רוצה ללמוד עלינו עוד</a>
            </div>
		</div>
    </div>
</div>
<div class="small-heroes">
	<div class="container">
		<div class="element-content row">
			<div class="s_hero col-lg-4 col-md-4">
				<?php 
					query_posts('name=buy-hero');
					have_posts();
					the_post();
					$buyFeaturedImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
				?>
				<div class="s-hero-img">
                    <a href="/קניית-דירה"><img src="<? echo $buyFeaturedImageUrl ?>" alt="" /></a>
                </div>
                <div class="s-hero-text">
					<?php 
						echo wpautop(do_shortcode( $post->post_content ), false);
						//the_content(); 
					?>
				</div>
				<a class="readmore-link" href="/קניית-דירה">להמשך קריאה</a>
			</div>
			<div class="s_hero col-lg-4 col-md-4">
				<?php 
					query_posts('name=rent-hero');
					have_posts();
					the_post();
					$letFeaturedImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
				?>
                <div class="s-hero-img">
                	<a href="/השכרת-דירה"><img src="<? echo $letFeaturedImageUrl ?>" alt="" /></a>
                </div>
                <div class="s-hero-text">
					<?php 
						echo wpautop(do_shortcode( $post->post_content ), false);
						//the_content(); 
					?>
                </div>
                <a class="readmore-link" href="/השכרת-דירה">להמשך קריאה</a>
            </div>
            <div class="s_hero col-lg-4 col-md-4">
            	<?php 
					query_posts('name=sell-hero');
					have_posts();
					the_post();
					$sellFeaturedImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
				?>
                <div class="s-hero-img">
                	<a href="/מכירת-דירה"><img src="<? echo $sellFeaturedImageUrl ?>" alt="" /></a>
                </div>
                <div class="s-hero-text">
					<?php 
						echo wpautop(do_shortcode( $post->post_content ), false);
						//the_content(); 
					?>
                </div>
                <a class="readmore-link" href="/מכירת-דירה">להמשך קריאה</a>
            </div>
		</div>
	</div>
</div>

<?php 
	query_posts('name=about-home');
	have_posts();
	the_post();
	$aboutFeaturedImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>
<div class="home-section about-content">
    <div class="container">
        <div class="element-content row">
            <div class="col-lg-7 col-lg-offset-1 col-md-7 col-md-offset-1 about-content-text">
                <?php 
                	//the_content();
  					
                	$content = apply_filters('the_content', get_the_content()); 
                	$content = apply_filters( 'get_the_excerpt', $content );
                	$content = wpautop(do_shortcode( get_the_content() ), false);
                	echo content_excerpt($content, 600);
                ?>
            </div>
            <div class="col-lg-4  col-md-4 ">
                <div class="hm-about-img">
                    <img src="<? echo $aboutFeaturedImageUrl ?>" />
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
get_template_part( 'ml-form-strip' );
query_posts('orderby=rand&showposts=1&cat='.get_cat_ID($testimonialCat)); 
while (have_posts()) : the_post();
	$featuredImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));

?>
<div class="home-section testimonials-content">
	<div class="container">
		<div class="element-content row">
			<div class="col-lg-1  col-md-2 col-sm-2 col-xs-3">
				<div class="hm-about-img">
					<img src="<? echo $featuredImageUrl ?>">
				</div>
			</div>
			<div class="col-lg-11 col-md-10 col-sm-10 col-xs-9">
				<?php
						the_title( '<h3>', '</h3>' );
						//echo '<p>';
						//the_content();
						//echo '</p>';

						echo wpautop(do_shortcode( $post->post_content ), false);
						//echo wpautop($post->post_content, false);
				?>
			</div>
		</div>
	</div>
</div>
<?php 
endwhile;
get_footer();
