<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Mashal_law
 */

global $pageType, $pageClass, $post;

if($pageClass) {
	$pageType = $pageClass.'-page';
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php
	wp_head(); 
?>
<!--[if lt IE 9]>
      <script src="<?php echo get_template_directory_uri()?>/js/html5shiv.min.js"></script>
      <script src="<?php echo get_template_directory_uri()?>/js/respond.min.js"></script>
<![endif]-->
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site <?php echo $pageType; ?>">
<a class="skip-link screen-reader-text" href="#content">
<?php _e( 'Skip to content', 'mashal-law' ); ?>
</a>
<header id="masthead" role="banner">
	<div class="container">
		<div class="pull-right logo-wrapper"> 
			<div class="logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="" />
				</a>
			</div>
			<div class="site-branding">
                <div class="row">
                    <?php if($pageType == 'home-page') {?>
                    <h1 class="col-lg-12 site-title">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo get_bloginfo('name'); ?></a>
                    </h1>
                    <?php } else { ?>
                    <h2 class="col-lg-12 site-title">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo get_bloginfo('name'); ?></a>
                    </h2>
                    <?php } ?>
                    <h2 class="col-lg-12 site-description"><?php echo get_bloginfo('description'); ?></h2>
                </div>
            </div>
		</div>
		
			<div class="pull-right top-nav">
				<nav id="site-navigation" class="main-navigation" role="navigation">
					<button class="menu-toggle">
					
					</button>
					<?php 
						wp_nav_menu( array( 'theme_location'  => '',
											'menu'            => '',
											'container'       => 'div',
											'container_class' => 'menu-main-menu-container',
											'container_id'    => '',
											'menu_class'      => 'menu',
											'menu_id'         => 'menu-main-menu',
											'echo'            => true,
											'fallback_cb'     => 'wp_page_menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
											'depth'           => 0,
											'walker'          => '' ) ); 
					?>
				</nav>
			</div>
		
	</div>
</header>