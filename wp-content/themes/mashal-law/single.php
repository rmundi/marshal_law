<?php
/**
 * The template for displaying all single posts.
 *
 * @package Mashal_law
 */
if(have_posts()) {
	the_post();
	$cats = wp_get_post_categories($post->ID);

	if($cats){
		foreach($cats as $c) {
			$cat = get_category( $c );
			if($cat->name == "testimonial") {
				global $wp_query;
			    $wp_query->is_404 = true;
			    $wp_query->is_single = false;
			    $wp_query->is_page = false;

			    include( get_query_template( '404' ) );
			    exit(2);
			}
		}
	}
}
rewind_posts();
get_header();

while ( have_posts() ) : the_post(); 
	get_template_part( 'content', 'single' );
endwhile; 

get_footer();