<?php
/**
 * Template Name: Mashal Law Testimonials
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
global $pageClass;

$pageClass = 'testimonials';

get_header(); 
?>
<?php get_template_part( 'ml-news-strip' ); ?>

<section class="section-content testimonials-section">
    <div class="container">
        <div class="article-title">
            <h1>
                לקוחות ממליצים
            </h1>
        </div>
    </div>
    <?php 
		query_posts( 'cat='.get_cat_ID('testimonial') );
		while (have_posts()) : the_post();
			$featuredImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
			?>
	<article class="testimonial-item">
        <div class="container">
            <div class="element-content row">
                <div class="col-lg-1  col-md-2 col-sm-2 col-xs-3">
                    <div class="testi-img-wrapper">
                        <img src="<? echo $featuredImageUrl ?>" alt="" />
                    </div>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-10 col-xs-9">
                    <h3>
                        <?php the_title(); ?>
                    </h3>
                    <p>
                        <?php
                            echo wpautop(do_shortcode( $post->post_content ), false); 
                            //the_content(); 
                        ?>
                    </p> 
                </div>
            </div>
        </div>
    </article>
		<?php endwhile; ?>
</section>
<?php 
get_template_part( 'ml-form-strip' );
get_template_part( 'ml-related-articles' );
get_footer();
