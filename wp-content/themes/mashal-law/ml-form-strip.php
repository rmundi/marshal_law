<?php
/**
 * The template for displaying a small signup strip - for signing up
 *
 * @package WordPress
 * @subpackage Marshal_Law
 * @since Twenty Fourteen 1.0
 */
?>
<div class="form-strip">
	<div class="container">
		<?php 
			$mashal_law_data = get_option('mashal_options');
			$mashal_law_data = json_decode($mashal_law_data);
			add_filter('the_content', 'fix_columns');
			if ( $mashal_law_data->signup_shortcode ) {
				$shortcode = stripslashes($mashal_law_data->signup_shortcode);
				echo apply_filters('the_content',$shortcode);
			} else {
				echo "Please setup the contact-7 form in mashal-law options";
			}
		?>	
	</div>
</div>