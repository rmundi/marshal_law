<?php
/**
 * Template Name: Mashal Law About
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
global $post, $pageClass;

$pageClass = 'about';

get_header(); 
?>
<?php get_template_part( 'ml-news-strip' ); ?>
<article class="article-content about-content">
     <!-- <div class="article-title">
      <div class="container">
            <h1>
                <?php the_title(); ?>
            </h1>
        </div>
    </div>-->
    <div class="article-text">
        <div class="container">
            <div class="element-content row">
                <div class="col-lg-7 col-lg-offset-1 col-md-7 col-md-offset-1">
                    <?php 
                        //apply_filters('the_content', the_content()); 
                        echo wpautop(do_shortcode( $post->post_content ), false);
                        $aboutFeaturedImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
                    ?>
                </div>
                <div class="col-lg-4  col-md-4 ">
                    <div class="hm-about-img">
                        <img src="<? echo $aboutFeaturedImageUrl ?>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>

<?php 
get_template_part( 'ml-form-strip' );
get_template_part( 'ml-related-articles' );
get_footer();
