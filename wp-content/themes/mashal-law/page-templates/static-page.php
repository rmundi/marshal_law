<?php
/**
 * Template Name: Mashal Law Static
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
global $post, $pageType, $pageClass;
remove_filter( 'the_content', 'wpautop' );

$pageContent = apply_filters('the_content', $post->post_content);

$pageTitle = $post->post_title;
$pageID = $post->ID;


$pageData = get_post_custom_values( 'page-class' );
if($pageData)
	$pageClass = $pageData[0];
else
	$pageClass = $post->post_name;

if(urldecode($pageClass) == 'השכרת-דירה')
    $pageClass = 'rent';
elseif(urldecode($pageClass) == 'מכירת-דירה')
    $pageClass = 'sell';
elseif(urldecode($pageClass) == 'קניית-דירה') 
    $pageClass = 'buy';

get_header();

$getClientsArgs = array(
    'name' => $pageType.'-post'
);

query_posts($getClientsArgs);
have_posts();
the_post();
$featuredImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
$title = ml_get_text(get_the_title());

get_template_part( 'ml-news-strip' );
?>
<div class="top-hero home">
	<div class="container">
		<div class="element-content row">
			<div class="hero-img col-lg-4 col-md-4 ">
                <a href="/category/<? echo str_replace('-page', '', $pageType) ?>/" rel="nofollow"><img src="<? echo $featuredImageUrl ?>" alt="" /></a>
            </div>
            <div class="hero-text col-lg-6 col-lg-offset-2 col-md-6 col-md-offset-2">
                <h2>
                    <?php echo $title; ?>
                </h2>
                <p>
                    <?php 
                        echo wpautop(do_shortcode( $post->post_content ), false);
                        //the_content(); 
                    ?>
                </p>
            </div>
		</div>
    </div>
</div>
<article class="article-content <?php echo $pageType; ?>-content">
	<div class="container">
		<?php echo $pageContent; ?>
		</div>
	</div>
</article>
<?php 
get_template_part( 'ml-form-strip' );
get_template_part( 'ml-related-articles' );
get_footer();
