<?php
/**
 * Template Name: Mashal Law Contact
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
global $post, $pageClass;

$pageClass = 'contact';

$pageContent = apply_filters('the_content', $post->post_content);
//[contact-form-7 id="132" title="Untitled"]

preg_match('/\[contact-form-7.*\]/i', $post->post_content, $matches);
if(isset($matches[0]) && $matches[0] != "") {
	$wpcf7 = $matches[0];
	$content = preg_replace('/\[contact-form-7.*\]/i', '', $post->post_content);
	$pageContent = apply_filters('the_content', $content);
	$wpcf7Content = apply_filters('the_content', $wpcf7);
}

get_header(); 
?>
<?php get_template_part( 'ml-news-strip' ); ?>
<article class="article-content contact-content">
    <div class="container">
        <div class="article-title">
            <h1>
                <?php the_title(); ?>
            </h1>
        </div>
        <div class="article-text">
            <?php 
                //echo $pageContent 
                echo wpautop(do_shortcode( $pageContent ), false); 
            ?>
        </div>
    </div>
    <div class="contact-form-element">
        <div class="container">
            <?php echo $wpcf7Content ?>
        </div>
    </div>
</article>
<?php
get_footer();
