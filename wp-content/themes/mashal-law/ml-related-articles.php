<?php
/**
 * The template for displaying related article posts
 *
 * @package Mashal_law
 */
global $pageType;
$mashal_law_data = get_option('mashal_options');
$mashal_law_data = json_decode($mashal_law_data);
$pageSlug = str_replace('-', '_', $pageType);

?>
<div class="page-section related-articles-wrapper">
    <div class="container">
        <h3>קרא עוד בבלוג שלנו</h3>
        <div class="related-articles element-content row">
<?php
if($pageSlug == 'testimonials_page' || $pageSlug == 'about_page') {
	query_posts('orderby=rand&showposts=3&cat='.get_cat_ID('buy').','.get_cat_ID('rent').','.get_cat_ID('sell')
					.','.get_cat_ID('קניית דירה').','.get_cat_ID('השכרת דירה').','.get_cat_ID('מכירת דירה'));
} else {
	if($pageSlug == 'buy_page')
		$catGroups = get_cat_ID('buy').','.get_cat_ID('קניית דירה');
	elseif($pageSlug == 'sell_page')
		$catGroups = get_cat_ID('sell').','.get_cat_ID('מכירת דירה');
	else
		$catGroups = get_cat_ID('rent').','.get_cat_ID('השכרת דירה');

	//query_posts('orderby=rand&showposts=3&cat='.$catGroups); 
	query_posts('orderby=rand&showposts=3&cat='.get_cat_ID('buy').','.get_cat_ID('rent').','.get_cat_ID('sell')
					.','.get_cat_ID('קניית דירה').','.get_cat_ID('השכרת דירה').','.get_cat_ID('מכירת דירה'));
}	
while (have_posts()) : the_post();
	$featuredImageUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
	if(!$featuredImageUrl) 
		$featuredImageUrl = get_template_directory_uri().'/images/related-article-ph.png';
?>
			<div class="related-content col-lg-3 col-md-3 col-sm-3">
			    <div class="related-img">
			        <a href="<?php echo get_permalink(); ?>">
			        	<?php 
						if ( has_post_thumbnail() ) { 
							echo get_the_post_thumbnail( get_the_ID(), 'post-thumbnail' ); 
						}
						?>
			        </a>
			    </div>
			    <h3 class="related-title">
			        <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
			    </h3>
			</div>
<?php endwhile; wp_reset_query(); ?>
		</div>
    </div>
</div>
