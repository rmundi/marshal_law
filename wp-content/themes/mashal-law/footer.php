<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Mashal_law
 */
$posts = wp_get_recent_posts((array(
        'numberposts' => 4, 
        'offset' => 0,
        'category' => get_cat_ID('מהבלוג שלנו'), 
        'orderby' => 'post_date',
        'order' => 'DESC', 
        'include' => '',
        'meta_key' => '',
        'meta_value' =>'', 
        'post_type' => 'post', 
        'post_status' => 'publish',
        'suppress_filters' => true
    )));

$mashal_law_data = get_option('mashal_options');
$mashal_law_data = json_decode($mashal_law_data);

$blogThreads = '<h3 class="footer-title">מהבלוג שלנו:</h3>';
foreach($posts as $post) {
	$blogThreads .='<div class=""><a href="'.get_permalink($post["ID"]).'"> '.$post["post_title"].'</a></div>';
}
?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="footer-content row">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<a href="<?php echo $mashal_law_data->facebook_url ; ?>" target="_blank"><div class="social-icons fb"></div></a>
					<a href="<?php echo $mashal_law_data->linkdin_url ; ?>" target="_blank"><div class="social-icons in"></div></a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<?echo $blogThreads; ?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<h3>צור קשר:</h3>
					<div>info@mashal-law.co.il</div>
					<div>החילזון 17, רמת גן</div>
					<div>טלפון: 7538384 03</div>
					<div>פקס: 7538355 03</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<h3>מפת אתר</h3>
					<div><a href="/מכירת-דירה">מכירת דירה</a></div>
					<div><a href="/קניית-דירה">קניית דירה</a></div>
					<div><a href="/השכרת-דירה">השכרת דירה</a></div>
				</div>
			</div>
			<div class="copyright"><p>כל הזכויות שמורות ארז משעל משרד עורכי דין &copy;, אין לעשות כל שימוש בתכנים מאתר זה ללא קבלת אישור מראש</p></div>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
