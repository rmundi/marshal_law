<?php
/**
 * @package Mashal_law
 */
global $isHot, $post, $isBuy, $isSell, $isGeneral, $isLet, $hottag;

$isBuy = false;
$isLet = false;
$isSell = false;
$isGeneral = false;
$isHot = has_tag( $hottag );

$cats = wp_get_post_categories($post->ID);

if($cats){
	foreach($cats as $c) {
		$cat = get_category( $c );
		if($cat->name == "buy" || $cat->name == "קניית דירה")
			$isBuy = true;
		elseif($cat->name == "rent" || $cat->name == "השכרת דירה")
			$isLet = true;
		elseif($cat->name == "sell" || $cat->name == "מכירת דירה")
			$isSell = true;
		elseif($cat->name == "general" || $cat->name == "טקדת" || $cat->name == "blog")
			$isGeneral = true;
		//elseif($cat->name == "hot")
		//	$isHot = true;
	}
}
?>
<?php get_template_part( 'ml-news-strip' ); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class( array('article-content', 'blogpost-content',) ); ?>>
	<div class="container">
		<div class="col-lg-4 col-md-4">
			<?php get_template_part( 'ml-blogform' ); ?>
        <div class="col-lg-8 col-md-8">
           <?php
				if($isHot)
					echo '<div class="hot-topic">חם בבלוג</div>';
				if ( is_single() ) :
					the_title( '<h2>', '</h2>' );
				else :
					the_title( '<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;

			?>
			<div class="entry-meta">
				<?php
					if ( 'post' == get_post_type() )
						echo '<div class="post-date">'.get_the_date('d.m.y', $post->ID).'</div>';

					if(has_post_thumbnail()) {
						the_post_thumbnail( );
					}
				?>
			</div>
			<div class="entry-content">
				<?php
					echo wpautop(do_shortcode( $post->post_content ), false);
					//if ( has_shortcode( $post->post_content, 'maincontent' )) { 
					//	echo apply_filters('the_content', $post->post_content);
					//} else { 
					//	echo wpautop($post->post_content, false);
					//} 
					
					//the_content();   
				?>
			</div>
        </div>
	</div>
</article>
<?php get_template_part( 'ml-form-strip' ); ?>
