<?php
/**
 * The template for displaying related article posts
 *
 * @package Mashal_law
 */
global $isBuy, $isSell, $isGeneral, $isLet;

if (is_category( )) {
	$category = get_query_var('cat');
	$yourcat = get_category ($category);
} else {
	// is blog post

}
?>
<div class="groups-title">
		<a href="/<?php echo strtolower(urlencode('מהבלוג-שלנו'));?>/"><h2>קטגוריות</h2></a>
	</div>
	<div class="groups-container">
		<a href="/<?php echo strtolower(urlencode('מהבלוג-שלנו'));?>/"><div class="<?php if($isGeneral) {echo "ticked";} ?>">ראשי</div></a>
		<?php
			$args = array(
				'type'                     => 'post',
				'child_of'                 => 0,
				'parent'                   => get_cat_ID('מהבלוג שלנו'),
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 1,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'category',
				'pad_counts'               => false 

			);  

			$categories = get_categories( $args );

			foreach($categories as $category) { ?>
				<a href="/<?php echo strtolower(urlencode(get_option( 'category_base' ))); ?>/<?php echo $category->category_nicename; ?>/"><div class="<?php 
					$catInfo = get_the_category();
					$superTick = false;
					if(!$isGeneral) {
						foreach($catInfo as $kitten) {
							if($kitten->category_nicename == $category->category_nicename) {
								$superTick = true;
							}
						}
					}
					
					if($category->category_nicename == $yourcat->slug || $superTick) { 
						echo "ticked"; 
					} 
				?>"><?php echo $category->cat_name; ?></div></a>
			<?php }	?>
	</div>
    <div class="form-container">
    <?php 
		$mashal_law_data = get_option('mashal_options');
		$mashal_law_data = json_decode($mashal_law_data);
		add_filter('the_content', 'fix_columns');
		if ( $mashal_law_data->sidebar_shortcode ) {
			$shortcode = stripslashes($mashal_law_data->sidebar_shortcode);
			echo apply_filters('the_content',$shortcode);
		} else {
			echo "Please setup the contact-7 form in mashal-law options";
		}
	?>
	</div>
</div>